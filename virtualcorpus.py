import threading
import re
from queue import  Queue #requires python3
from time import sleep
from itertools import groupby, count, chain
import os
from timeit import default_timer

WORDS = "./wordLemPoS.txt"
DATABASE = "./database.txt"
SOURCE = "./sources.txt"
TREEDUMP = "./storagecorpus.dat"
TREEDUMP2 = "./storagecorpus2.dat"


class LetterTree:
 
    class WordEntity:
        def __init__(self):
            self.IsCompleteWord = False #Used to track with this is only a subset of an actual word
            self.ArticlesCountContext = {} #contains the article as key and the counts and context as a dictionary value
            self.TotalCount = 0 #Total frequencies over all articles
            self.ChildLetters = []
            self.WordEnglish = []
        def AddTranslation(self, context, word):
            self.WordEnglish.extend([word])
        def HasWord(self, word):
            for key, val in self.ArticlesCountContext.items():
                if(word in val[1].split(":")):
                    return True
            return False
        def HasWordEng(self, word):
            if(word in self.WordEnglish.values()):
                return self
            else:
                return None
        def GetContexts(self):
            return  self.WordEnglish
        def CompleteWord(self):
            return self.IsCompleteWord #Determine word status
        def GetLetter(self):
            return self.LastLetter
        def GetAllChildren(self):
            return self.ChildLetters
        def GetCompleteChildren(self):
            temp = []
            for child in self.ChildLetters:
                if child.CompleteWord():
                    temp.append(child)
            return temp
        def GetChildWith(self, letter, create, queue=None):
            i = 0
            for childletter in self.ChildLetters:
                if childletter.GetLetter() == letter:
                    if(queue):
                        queue.put(self.ChildLetters[i])
                        return True
                    return self.ChildLetters[i]
                i += 1
            if create:
                if(queue):
                    s = self.AddChildLetter(letter)
                    queue.put(s)
                    return True
                return self.AddChildLetter(letter)
            else:
                return None
        def SetLetter(self,letter):
            self.LastLetter = letter
        def AddChildLetter(self, letter):
            child = type(self)()
            child.SetLetter(letter)
            self.ChildLetters.append(child)
            return self.ChildLetters[-1]
        def HasChildren(self):
            return len(self.ChildLetters)
        def SetWord(self, word):
            self.Word = word
        def AddWordInstance(self, Article, context):
            if Article in self.ArticlesCountContext:
                self.ArticlesCountContext[Article][0] += 1
                self.ArticlesCountContext[Article][1] += ":"+context
            else:
                self.IsCompleteWord = True
                self.ArticlesCountContext[Article] = [1, context]
            return None
        def RemoveArticle(self, article):
            try:
                del self.ArticlesCountContext[article]
            except:
                print("Failed to remove Article")
            return True
        def UpdateTotalCount(self):
            self.TotalCount=0
            for Count, Context in self.ArticlesCountContext.values():
                self.TotalCount += Count
            return self.TotalCount
        def GetArticlesAndCounts(self):
            return self.ArticlesCountContext
        def UpdateContexts(self):
            return None
        def OverrideArticleAndCounts(self, articleAndCount):
            if(len(articleAndCount) >= 1):
                for i in range(len(articleAndCount)):
                    self.ArticlesCountContext[list(articleAndCount.keys())[i]] = list(articleAndCount.values())[i]
                self.IsCompleteWord = True
            else:
                return False
        def OverrideContexts(self, contexts):
            if(len(contexts) >=1):
                self.WordEnglish = contexts
           
    def AddCompositeWordToQueue(self, word, article, context):
        return self.AddWordToQueue(word, article,context)
    def GetCountFromTemp(self, tempitem):
        return tempitem[1]
    def __init__(self):       
        self.Letters = ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
        self.RootLetters = []
        self.WordQueue = []
        self.RootLetters2 = []
        self.AllowQueueModification = True
        self.TotalWordsAdded = 0
        self.TopWordsMutex = threading.Lock()
        self.FindWordsMutex = threading.Lock()
        self.MaxThreads =2000
        self.WordThread = None
        self.que = Queue()
        self.NumWordsPerThread = 400
        self.WriteFileMutex = threading.Lock()

        for letter in self.Letters:
            RootLetter =self.WordEntity()
            RootLetter.SetLetter(letter)
            self.RootLetters.append(RootLetter)
            RootLetter2 = self.WordEntity()
            RootLetter2.SetLetter(letter)
            self.RootLetters2.append(RootLetter2)
        
    def __Write(self, text):
        self.WriteFileMutex.acquire()
        self.TreeFile.write(text + "\n")
        self.WriteFileMutex.release()
    def __StoreTree(self, Letter, current, starting=False, which = 0):
        dashes = re.sub("[0-9]", "", current)
        wordSoFar = current
        childrenLetters = None
        number = threading.activeCount()

        if(starting):
            if(not which):
                self.__Write(wordSoFar + "\t" + str(Letter[0].GetArticlesAndCounts()))
            else:
                self.__Write(wordSoFar  + "\t" + str(Letter[0].GetContexts()))
            self.__Write("\n")
            childrenLetters=Letter[0].GetAllChildren()
        else:
            childrenLetters = Letter
        for child in childrenLetters:
            wordSoFar2 = "-"*(len(dashes)+1)+child.GetLetter()
            if(not which):
                self.__Write(wordSoFar2 + "\t" + str(child.GetArticlesAndCounts()))
            else:
                self.__Write(wordSoFar2  + "\t" + str(child.GetContexts()))

            self.__Write("")
            if(child.HasChildren()):
                t = threading.Thread(target=self.__StoreTree,  args=(child.GetAllChildren(),wordSoFar2))
                t.start()
                while(t.isAlive()):
                    sleep(0.01)

        return True
    def StoreTree(self, which = 0):
        if(not which):
            self.TreeFile = open(TREEDUMP, mode = "w", buffering=1)
        else:
            self.TreeFile = open(TREEDUMP2,mode="w",buffering=1)
        for aLetter in self.Letters:
            if(not which):
                letterEntity = self.RootLetters[self.Letters.index(aLetter)]
            else:
                letterEntity = self.RootLetters2[self.Letters.index(aLetter)]
            self.__StoreTree([letterEntity, ], aLetter, True, 1)

        return True
    def ReadTreeDump(self, which=0):
        if(not which):
            self.TreeFileRead = open(TREEDUMP, mode ="r")
        else:
            self.TreeFileRead = open(TREEDUMP2, mode ="r")

        index = 0
        regex = re.compile(r'^[0-9][\[\]\{\}\-\,\'\t\n\:0-9 ]+?\n[0-9]', re.MULTILINE)
        text = self.TreeFileRead.read()
        thr = []
        for i in range(8):
            x= regex.search(text, index)
            if(x):
                oldindex = index
                index = x.end() -1
                difference = index - oldindex
                thr.append(threading.Thread(target =self.__ReadTreeDump, args=((x.group(0)[:difference], ))))
                thr[-1].start()
                while(thr[-1].isAlive()):
                    pass
        text = ""
        return True
            
    def __ReadTreeDump(self, text, which=0):
        lastLetters = []
        dashes = [0]
        text2 = text.split("\n")
        for line in text2:
            if(re.search("^[^\-0-9]", line)):
                print("Skipping")
                continue
            value =re.search("-+", line)
            if (value != None):
                lengthDashes = len(value.group(0)) #Determine number dashes for level of heirarchy
            fields = line.split("\t")
            if(len(fields) > 2):
                print("Too Many Fields")
            if(re.search("^[0-9]", line)):
                lastLetters = []
                lastLetters.append(self.RootLetters[self.Letters.index(fields[0])])
                if(len(fields) == 2):
                    if(not which):
                        lastLetters[-1].OverrideArticleAndCounts(eval(fields[1]))
                    else:
                        lastLetters[-1].OverrideContexts(eval(fields[1]))
                continue
            elif len(fields) == 2:
                if(lengthDashes < dashes[-1]):
                    lastLetters=lastLetters[:lengthDashes]
                dashes.append(lengthDashes)
                lastLetters.append(lastLetters[-1].GetChildWith(fields[0][-1], True))
                #print("after %d" % len(lastLetters))
                if(not which):
                    lastLetters[-1].OverrideArticleAndCounts(eval(fields[1]))
                else:
                    lastLetters[-1].OverrideContexts(eval(fields[2]))
            else:
                continue
        return True
    def ReadDatabase(self, which = 0):
        self.que = Queue()
        if(not which):
            Reader =open(DATABASE,mode = "r", encoding="utf-8", errors="replace")   
        else:
            Reader = open(WORDS, mode = "r",encoding="utf-8", errors="replace")
        try:            
            self.ReadParallel(chain(Reader,iter([])), self, self.que, which).start()
            WordThread = threading.Thread(target=self.__StartProcessWordQueue, args=([which]))
            WordThread.start()
            while(WordThread.isAlive()):
                sleep(0.6)
            #self.__StartProcessWordQueue(which)
        except Exception as Error:
            print("Error in Read Database:")
            print(Error)
        return True
    def TopWords(self):
        length = 0
        self.__TopWords()
        self.temp = sorted(self.temp, key = self.GetCountFromTemp, reverse=True)
        if(len(self.temp) < 100):
            length = len(self.temp)
        else:
            length = 100
        return self.temp[:length]
    def __TopWords(self, IterableLetter=None, partialword=""):
        if(IterableLetter == None):
            IterableLetter = self.RootLetters
        if(partialword == ""):
            self.temp = []
        pword = partialword  
        for letter in IterableLetter:

            if(letter.CompleteWord()):
                self.TopWordsMutex.acquire()
                articlesCounts = letter.GetArticlesAndCounts()
                self.temp.append([pword+letter.GetLetter(), letter.UpdateTotalCount(), len(articlesCounts)])
                self.TopWordsMutex.release()
            if(letter.HasChildren()):
                descendantWord = pword+letter.GetLetter()
                t = threading.Thread(target=self.__TopWords, args=(letter.GetAllChildren(), descendantWord))
                t.start()
                while(t.isAlive()):
                    sleep(0.05)



                
    def __StartProcessWordQueue(self, which = 0): 
        while True:
            try:
                word = self.que.get()
                if(len(word) == 1):
                    print("Stopping")
                    self.que.task_done()
                    break
                if( len(word) >= 3):
                    print("Added word %d word- %s" % (self.TotalWordsAdded, word))
                    self.TotalWordsAdded += 1
                    if(not which):
                        self.AddWord(word[0], word[1], word[2])
                    elif which:
                        self.AddWord2(word[0],word[2])
                else:
                    print("Error 1: %s" % word)
            except Exception as ex:
                print(ex)
                print(which)
            
            self.que.task_done()
        return True
    def AddWordToContext(self, word):
        pass
    def AddWord2(self,context, engword):
        length = len(str(context))
        succeeded = None
        index = 0
        for i in range(length):
            if(i == length-1 and length >1):
                break            
            if(i ==0 ):
                try:
                    index =self.Letters.index(str(context)[i])
                except Exception:
                    print("Error")
                    continue
                if length > 1:
                    succeeded = self.RootLetters2[index].GetChildWith(str(context)[i+1], True)
                else:
                    succeeded = self.RootLetters2[index]
            else:
                succeeded = succeeded.GetChildWith(str(context)[i+1], True)
                
        if(succeeded):
            succeeded.AddTranslation(context,engword)
            return True
        else:
            
            return False
    def AddWord(self, word, article, context):
        length = len(str(word))
        succeeded = None
        index = 0
        for i in range(length):
            if(i == length-1 and length >1):
                break            
            if(i ==0 ):
                try:
                    index =self.Letters.index(str(word)[i])
                except Exception:
                    print("Error")
                    continue
                if length > 1:
                    succeeded = self.RootLetters[index].GetChildWith(str(word)[i+1], True)
                else:
                    succeeded = self.RootLetters[index]
            else:
                succeeded = succeeded.GetChildWith(str(word)[i+1], True)
                
        if(succeeded):
            succeeded.AddWordInstance(article, context)
            return True
        else:
            
            return False
    def FindWordEng(self, word2,which = 0):
        start = default_timer()
        self.temp = None
        if(not which):
            self.__FindWordEng(word =  word2)
        else:
            self.__FindWordEng(word = word2,which=1)
        print("time: %f" % (default_timer()-start))
        if(self.temp):
            return self.temp[0]
        else:
            return None

    def __FindWordEng(self, IterableLetter=None, partialword="", word=None,which = 0):
        if(IterableLetter == None):
            if(not which):
                IterableLetter = self.RootLetters
            else:
                IterableLetter = self.RootLetters2

        if(partialword == ""):
            self.temp = []
        pword = partialword
        if(word == None):
            return False
        for letter in IterableLetter:
            if(len(self.temp)):
                break
            if which:
                if(letter.HasWordEng(word)):
                    self.FindWordsMutex.acquire()
                    self.temp.append(letter)
                    self.FindWordsMutex.release()
                    return True
            else:
                if(letter.HasWord(word)):
                    self.FindWordsMutex.acquire()
                    self.temp.append(letter)
                    self.FindWordsMutex.release()
                    return True
                
            if(letter.HasChildren()):
                descendantWord = pword+letter.GetLetter()
                t = threading.Thread(target = self.__FindWordEng,  args = (letter.GetAllChildren(), descendantWord, word))
                t.start()
                while(t.isAlive()):
                    sleep(0.05)
        return False
    def FindWord(self,  word):
        length = len(word)
        succeeded = False
        for i in range(length):
            if (i == length -1 or succeeded== None) and length !=1:
                break
            index = 0
            if(i ==0 ):
                index =self.Letters.index(word[i].upper())
                if(length != 1):
                    succeeded = self.RootLetters[index].GetChildWith(word[i+1].upper(), False)
                else:
                    return self.RootLetters[index]
            else:
                succeeded = succeeded.GetChildWith(word[i+1].upper(), False)
        return succeeded
    def GetWordFrequency(self, word, article = None):
        succeeded2 = self.FindWord(word)
        if(succeeded2 and article == None):
           return succeeded2.UpdateTotalCount()
        elif(succeeded2 and article):
            if(article in succeeded2.GetArticlesAndCounts().keys()):
                return succeeded2.GetArticlesAndCounts()[article][0]
            else:
                return 0
        return 0
    def GetArticleAndWordFrequency(self, word):
        succeeded2 = self.FindWord(word)
        if(succeeded2):
            return succeeded2.GetArticlesAndCounts()
        return None
        
    class ReadParallel(threading.Thread):
        def __init__(self, sequence, object, queue, which =0, end=0):
            self.Reader = sequence
            self.tree = object
            super().__init__()
            self.Que = queue
            self.which = which
            self.end = end
        def run(self): 
            fields2  = ["Start"]
        
            while(len(fields2)):
                if(fields2[0] == "Start"):
                    fields2 = []
                try:
                    line = next(self.Reader)
                except StopIteration:
                    self.Que.put(["Done"])
                    return True
                if(not self.which):
                    if(re.search("[a-zA-Z:\-;]+", line) or line == ""):
                        print("Error")
                        fields2  = ["Start"]
                        continue
                    line = re.sub('\\n', "", line)
                else:
                    if(re.search("^[^0-9]", line)):
                        print("Skipping")
                        fields2  = ["Start"]
                        continue
                fields =  line.split()
                if(not self.which):
                    if(len(fields) ==3):
                        fields2.extend(fields)
                    else:
                        print(line)
                        print("EOF or Corrupt %s" % line)
                        fields2  = ["Start"]
                        continue
                else:
                    if(len(fields) !=5 and len(fields) !=4 and len(fields) !=3):
                        print(fields)
                        print("Error")
                        fields2  = ["Start"]
                        continue
                try:
                    
                    if(not self.which):
                        self.Que.put([fields2[2], fields2[0], fields2[1]])
                        fields2= ["Start"]
                    else:
                        if(len(fields) == 5):
                            self.Que.put([fields[1], fields[0], fields[2], fields[4]])
                        elif(len(fields) == 4 or len(fields) ==3):
                            self.Que.put([fields[1], fields[0], fields[2]])
                        fields2  = ["Start"]

                except Exception as ex:
                    print(self.which)
                    print(ex)
#                finally:
#                    fields2 = ["Start"]
                    
                    #self.Que.put(None)
            return True
class CorpusControl:
    
    def __init__(self):
        self.WordIDs = LetterTree()
        self.Commands = open("./commands", 'r')
        self.Output = open("./topwords.dat", "w")
        self.TempData = open("temp","w" )
        self.LastWord = ""
        if(os.path.isfile(TREEDUMP) and os.path.isfile(TREEDUMP2)):
            self.WordIDs.ReadTreeDump()
            self.WordIDs.ReadTreeDump(1)
        else:
            self.WordIDs.ReadDatabase()
            self.WordIDs.StoreTree()
            self.WordIDs.ReadDatabase(1)
            print("Done Reading going to Store")
            self.WordIDs.StoreTree(1)
        self.Output.write("Top Words" + "----"*5)
        self.Output.write("Word #Total  #Articles")
        for elm in self.WordIDs.TopWords():
            word = self.WordIDs.FindWordEng(elm[0],1)
            if(word):
                self.Output.write("%d\t%d\t%d" % (word.GetContexts.keys()[0], elm[1], elm[2]))
                self.TempData.write("Word Contexts")
        self.Done = open("Done","r+")
        while True:
            #input is the word in english
            value = next(self.Commands)
            value = value.split()
            if(value[0] == "q"):
                break
            word = self.WordIDs.FindWordEng(value[0],1)
            word = self.WordIDs.FindWordEng(word.GetContexts.keys[0],0)
            if(word):
                contexts = word.GetArticlesAndCounts()
            self.TempData.write("Word\tArticle\t\Context")
            for art, contx in contexts:
                for indiv in contx:
                    self.TempData.write(value[0] + "\t" + self.GetArticle(art) +"\t" + indiv)
        self.Commands.close()
        self.Output.close()
        self.TempData.cose()
    def GetArticle(self, articleID):
        self.Sources = open(SOURCE, "r")
        for line in self.Sources:
            sourceFields = line.split()
            if(len(sourceFields != 3)):
                continue
            if(sourceFields[0] == articleID):
                return sourceFields[2]
        self.Sources.close()
        return ""
    def GetWord(self, wordID):
        wordEntityAAC= self.Words.FindWord(wordID).GetArticlesAndCounts()
        self.Sources = open(WORDS, "r")
        groups = groupby(self.Sources, key=lambda aKey, lineIter=count(): next(lineIter)//1000)
        t = []
        for k, group in groups:
            t.append(threading.Thread(target=self.__GetWord, args=(group, wordEntityAAC)))
            t[-1].start()
        for thread in t:
            thread.join()
        self.Sources.close()
    
    def __GetWord(self, seq, AAC):
        for line in self.Sources:
            sourceFields = line.split()
            if(len(sourceFields) <3):
                continue
            if(sourceFields[1] in AAC):
                self.LastWord = sourceFields[2]
                return True
        return False
        
cc = CorpusControl()
        
        
